var http = require("http");
var server = http.createServer(function(request, response) {  //Creating the server with a callback function
    response.writeHead(200, { "Content-Type": "text/plain" });   //writing a header of response

    //It is used to send the response to the client. Whatever is written into it is directly sent as response to the client.
    response.write("Hello World"); 


    response.end();     // Signals the server that the content and the header have been completely sent to the client
  })
  server.listen(8888);

