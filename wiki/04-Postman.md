# What is Postman?

Postman is currently one of the most popular tools used in API testing. API stands for Application Programming Interface which allows software applications to communicate with each other via API calls.

For using the postman, you need to install the `Postman` application on your system.

Using `POSTMAN`, we can test our application by sending any type of request and then analyzing its response. We will learn how to send GET and POST types of requests over a `url` which will send a JSON object in response. We can then analyze that response using `postman`.

## Working with GET Requests

Get requests are used to retrieve information from the given URL. When a GET request is made, the data is retrieved. We can make a GET request directly just by entering the path. Also consider that in GET requests, no changes can be made in the actual object sent in response to the client.

We will use the following URL for all examples in this tutorial. It is a path to an API which will return a JSON object.

```
https://jsonplaceholder.typicode.com/users
```

Open the application and we will learn to send a GET request for the above API link.

- In the request URL field, input link
- Click Send
- You will see 200 OK Message
- There should be 10 user results in the body which indicates that your request has been successfully processed.

![alt text](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/postman_get1.png)

**\*Note**: There may be cases that Get request may be unsuccessful. It can be due to an invalid request URL or authentication is needed.

## Working with POST Requests

Post requests are different from Get request as the data can be modified using a POST request. For example, we will add a new user using a POST request.

Step 1) Click a new tab to create a new request.

![alt text](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/post1.png)

Step 2) In the new tab

- Set your HTTP request to POST.
- Input the same link in request url: https://jsonplaceholder.typicode.com/users
- switch to the Body tab

![alt text](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/post2.png)

Step 3) In Body,

- Click raw
- Select JSON

![alt text](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/post3.png)

Step 4) Copy and paste just one user result from the previous get request like below. Ensure that the code has been copied correctly with paired curly braces and brackets. Change id to 11 and name to any desired name. You can also change other details like the address.

```js
[
    {
        "id": 11,
        "name": "Krishna Rungta",
        "username": "Bret",
        "email": "Sincere@april.biz",
        "address": {
            "street": "Kulas Light",
            "suite": "Apt. 556",
            "city": "Gwenborough",
            "zipcode": "92998-3874",
            "geo": {
                "lat": "-37.3159",
                "lng": "81.1496"
            }
        },
        "phone": "1-770-736-8031 x56442",
        "website": "hildegard.org",
        "company": {
            "name": "Romaguera-Crona",
            "catchPhrase": "Multi-layered client-server neural-net",
            "bs": "harness real-time e-markets"
        }
    }
]
```

![alt text](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/post4.png)

**\*Note**: Post request should have the correct format to ensure that requested data will be created. It is a good practice to use Get first to check the JSON format of the request. Also usually you may require authentication to alter data in an application using a POST request.

![alt text](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/post5.png)

Step 5) Next,

- Click Send.
- Status: 201 Created should be displayed
- Posted data are showing up in the body.

![alt text](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/post6.png)